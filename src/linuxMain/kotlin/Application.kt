package org.example.gtk_notifier

import gtk3.*
import kotlinx.cinterop.CFunction
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import kotlin.system.exitProcess

@Suppress("EXPERIMENTAL_UNSIGNED_LITERALS", "EXPERIMENTAL_API_USAGE")
internal class Application(val id: String) {
    val appPtr by lazy {
        if (id.trim().isEmpty()) {
            g_printerr("GTK Application ID cannot be empty!\n")
            exitProcess(-1)
        }
        gtk_application_new(id, G_APPLICATION_FLAGS_NONE)!!
    }

    fun run(): Int = g_application_run(appPtr.reinterpret(), 0, null)

    fun connectActivateSignal(slot: CPointer<CFunction<(app: CPointer<GtkApplication>, userData: gpointer) -> Unit>>,
                              data: gpointer): ULong =
        connectGtkSignal(obj = appPtr, signal = "activate", slot = slot, data = data)

    fun use(init: Application.() -> Unit) {
        this.init()
        g_object_unref(appPtr)
    }
}
