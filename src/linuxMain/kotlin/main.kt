@file:Suppress("EXPERIMENTAL_UNSIGNED_LITERALS")

package org.example.gtk_notifier

import gtk3.*
import kotlinx.cinterop.*
import org.example.gtk_notifier.window.MainWindow

internal val app = Application("org.example.gtk-notifier")

fun main() {
    app.use {
        val mainWin = MainWindow(this)
        connectActivateSignal(staticCFunction(::activateApplication),
            mainWin.fetchReference())
        val status = run()
        mainWin.disposeStableRef()
        g_print("Application Status: %d", status)
    }
}

private fun activateApplication(@Suppress("UNUSED_PARAMETER") app: CPointer<GtkApplication>,
                                userData: gpointer) {
    val mainWin = userData.asStableRef<MainWindow>().get()
    mainWin.createUi {
        title = "GTK Notifier"
        borderWidth = 10u
        val mainLayout = createMainLayout()
        if (mainLayout != null) addWidget(mainLayout)
        isVisible = true
    }
}
