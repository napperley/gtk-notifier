package org.example.gtk_notifier.layout

import gtk3.GtkContainer
import gtk3.gtk_container_get_border_width
import gtk3.gtk_container_set_border_width
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.example.gtk_notifier.widget.Widget

@Suppress("EXPERIMENTAL_API_USAGE")
internal interface Container : Widget {
    val containerPtr: CPointer<GtkContainer>?
        get() = widgetPtr?.reinterpret()
    var borderWidth: UInt
        set(value) = gtk_container_set_border_width(containerPtr, value)
        get() = gtk_container_get_border_width(containerPtr)
}
