package org.example.gtk_notifier.layout

import gtk3.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.example.gtk_notifier.widget.Widget

@Suppress("EXPERIMENTAL_API_USAGE", "EXPERIMENTAL_UNSIGNED_LITERALS")
internal class BoxLayout(orientation: GtkOrientation = GtkOrientation.GTK_ORIENTATION_HORIZONTAL) : Container {
    override val containerPtr: CPointer<GtkContainer>? =
        gtk_box_new(orientation, 0)?.reinterpret()
    override val widgetPtr: CPointer<GtkWidget>? = containerPtr?.reinterpret()
    var spacing: Int
        set(value) = gtk_box_set_spacing(containerPtr?.reinterpret(), value)
        get() = gtk_box_get_spacing(containerPtr?.reinterpret())
    var uniformSizedChildren: Boolean
        set(value) = gtk_box_set_homogeneous(containerPtr?.reinterpret(), if (value) TRUE else FALSE)
        get() = gtk_box_get_homogeneous(containerPtr?.reinterpret()) == TRUE

    /**
     * With a vertical box a [Widget] is added to the *top* of the GTK Box. If the box is horizontal then a
     * [Widget][Widget] is added to the *left* of the GTK Box.
     * @param widget The [Widget] to prepend.
     * @param fill If *true* then the added [widget] will be sized to use all of the available space.
     * @param expand If *true* then the added [widget] will be resized every time the GTK Box is resized.
     * @param padding The amount of padding to use for the [widget] which is in pixels. By default no padding is used.
     */
    fun prependWidget(
        widget: Widget,
        fill: Boolean = true,
        expand: Boolean = false,
        padding: UInt = 0u
    ): Unit = gtk_box_pack_start(
        box = containerPtr?.reinterpret(),
        child = widget.widgetPtr,
        expand = if (expand) TRUE else FALSE,
        fill = if (fill) TRUE else FALSE,
        padding = padding
    )
}

internal fun boxLayout(orientation: GtkOrientation = GtkOrientation.GTK_ORIENTATION_HORIZONTAL,
                       init: BoxLayout.() -> Unit): BoxLayout {
    val layout = BoxLayout(orientation)
    layout.init()
    return layout
}
