@file:Suppress("EXPERIMENTAL_UNSIGNED_LITERALS", "EXPERIMENTAL_API_USAGE")

package org.example.gtk_notifier.widget

import gtk3.*
import kotlinx.cinterop.CFunction
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString
import org.example.gtk_notifier.connectGtkSignal

internal class ButtonWidget : Widget {
    override val widgetPtr: CPointer<GtkWidget>? = gtk_button_new()
    var label: String
        set(value) = gtk_button_set_label(widgetPtr?.reinterpret(), value)
        get() = gtk_button_get_label(widgetPtr?.reinterpret())?.toKString() ?: ""

    fun connectClickedSignal(slot: CPointer<CFunction<(app: CPointer<GtkButton>, data: gpointer) -> Unit>>,
                             data: gpointer): ULong = if (widgetPtr != null) {
        connectGtkSignal(obj = widgetPtr, slot = slot, data = data, signal = "clicked")
    } else {
        0uL
    }
}

internal fun buttonWidget(init: ButtonWidget.() -> Unit): ButtonWidget {
    val widget = ButtonWidget()
    widget.init()
    return widget
}
