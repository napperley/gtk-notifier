package org.example.gtk_notifier.widget

import gtk3.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString

internal class EntryWidget : Widget {
    override val widgetPtr: CPointer<GtkWidget>? = gtk_entry_new()
    var text: String
        set(value) = gtk_entry_set_text(widgetPtr?.reinterpret(), value)
        get() = gtk_entry_get_text(widgetPtr?.reinterpret())?.toKString() ?: ""
    var maxLength: Int
        set(value) = gtk_entry_set_max_length(widgetPtr?.reinterpret(), value)
        get() = gtk_entry_get_max_length(widgetPtr?.reinterpret())
    var widthChars: Int
        set(value) = gtk_entry_set_width_chars(widgetPtr?.reinterpret(), value)
        get() = gtk_entry_get_width_chars(widgetPtr?.reinterpret())
    var placeholderText: String
        set(value) = gtk_entry_set_placeholder_text(widgetPtr?.reinterpret(), value)
        get() = gtk_entry_get_placeholder_text(widgetPtr?.reinterpret())?.toKString() ?: ""
}

internal fun entryWidget(init: EntryWidget.() -> Unit): EntryWidget {
    val widget = EntryWidget()
    widget.init()
    return widget
}
