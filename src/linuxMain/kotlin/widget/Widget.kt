package org.example.gtk_notifier.widget

import gtk3.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

internal interface Widget {
    val widgetPtr: CPointer<GtkWidget>?
    var vExpand: Boolean
        set(value) = gtk_widget_set_vexpand(widgetPtr, if (value) TRUE else FALSE)
        get() = gtk_widget_get_vexpand(widgetPtr) == TRUE

    fun grabFocus() {
        gtk_widget_grab_focus(widgetPtr?.reinterpret())
    }
}
