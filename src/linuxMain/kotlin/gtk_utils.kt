@file:Suppress("EXPERIMENTAL_UNSIGNED_LITERALS", "EXPERIMENTAL_API_USAGE")

package org.example.gtk_notifier

import gtk3.*
import kotlinx.cinterop.CFunction
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

/**
 * Connects a signal (event) to a slot (event handler). Note that all callback parameters must be primitive types or
 * nullable C pointers.
 * @param obj The object to use for connecting a [signal][signal] to a [slot][slot].
 * @param signal The name of the signal to connect to.
 * @param slot The slot to use for handling the signal.
 * @param data User data to pass through to the [slot]. By default no user data is passed through.
 * @param connectFlags The flags to use.
 * @return The handler ID for the [slot].
 */
internal fun <F : CFunction<*>> connectGtkSignal(
    obj: CPointer<*>,
    signal: String,
    slot: CPointer<F>,
    data: gpointer? = null,
    connectFlags: GConnectFlags = 0u
): ULong = g_signal_connect_data(
        instance = obj,
        detailed_signal = signal,
        c_handler = slot.reinterpret(),
        data = data,
        destroy_data = null,
        connect_flags = connectFlags
    )

fun sendNotification(id: String, title: String, body: String) {
    val notification = g_notification_new(title)
    g_notification_set_body(notification, body)
    g_application_send_notification(application = app.appPtr.reinterpret(), notification = notification,
        id = id)
}
