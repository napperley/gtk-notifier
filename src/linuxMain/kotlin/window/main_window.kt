package org.example.gtk_notifier.window

import gtk3.GtkButton
import gtk3.gpointer
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.StableRef
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.staticCFunction
import org.example.gtk_notifier.Application
import org.example.gtk_notifier.StableRefExtension
import org.example.gtk_notifier.layout.boxLayout
import org.example.gtk_notifier.sendNotification
import org.example.gtk_notifier.widget.buttonWidget
import org.example.gtk_notifier.widget.entryWidget

internal class MainWindow(app: Application) : AppWindow(app), StableRefExtension {
    override val stableRef = StableRef.create(this)
    private val titleTxt by lazy { createTitleTxt() }
    private val bodyTxt by lazy { createBodyTxt() }
    val notificationTitle
        get() = titleTxt.text
    val notificationBody
        get() = bodyTxt.text

    override fun fetchReference() = stableRef.asCPointer()

    override fun disposeStableRef() {
        stableRef.dispose()
    }

    override fun createMainLayout() = boxLayout {
        spacing = 2
        uniformSizedChildren = false
        prependWidget(titleTxt)
        prependWidget(bodyTxt)
        prependWidget(createNotifyBtn())
        resetFocus()
    }

    private fun createTitleTxt() = entryWidget {
        val maxChars = 25
        maxLength = maxChars
        widthChars = maxChars
        placeholderText = "Title"
    }

    private fun createBodyTxt() = entryWidget {
        val maxChars = 25
        maxLength = maxChars
        widthChars = maxChars
        placeholderText = "Body"
    }

    private fun createNotifyBtn() = buttonWidget {
        label = "Show Notification"
        vExpand = false
        connectClickedSignal(staticCFunction(::notifyBtnClicked), fetchReference())
    }

    override fun resetFocus() {
        titleTxt.grabFocus()
    }
}

private fun notifyBtnClicked(@Suppress("UNUSED_PARAMETER") widget: CPointer<GtkButton>, userData: gpointer) {
    val mainWin = userData.asStableRef<MainWindow>().get()
    sendNotification(id = "test", title = mainWin.notificationTitle, body = mainWin.notificationBody)
    mainWin.resetFocus()
}
