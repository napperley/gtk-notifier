package org.example.gtk_notifier.window

import gtk3.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.example.gtk_notifier.Application

internal abstract class AppWindow(private val app: Application) : WindowBase {
    private var _widgetPtr: CPointer<GtkWidget>? = null
    override val widgetPtr: CPointer<GtkWidget>?
        get() = _widgetPtr
    override val winPtr: CPointer<GtkWindow>?
        get() = widgetPtr?.reinterpret()

    override fun createUi(init: WindowBase.() -> Unit) {
        _widgetPtr = gtk_application_window_new(app.appPtr)
        this.init()
    }
}
