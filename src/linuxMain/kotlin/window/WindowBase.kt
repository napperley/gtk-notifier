package org.example.gtk_notifier.window

import gtk3.*
import kotlinx.cinterop.*
import org.example.gtk_notifier.layout.Container
import org.example.gtk_notifier.widget.Widget

internal interface WindowBase : Container {
    val winPtr: CPointer<GtkWindow>?
    var title: String
        set(value) = gtk_window_set_title(winPtr, value)
        get() = gtk_window_get_title(winPtr)?.toKString() ?: ""
    var isVisible: Boolean
        set(value) {
            if (value) gtk_widget_show_all(widgetPtr)
            else gtk_widget_hide(widgetPtr)
        }
        get() = gtk_widget_is_visible(widgetPtr) == TRUE

    fun addWidget(widget: Widget) = gtk_container_add(containerPtr, widget.widgetPtr)

    fun resetFocus() {}

    fun createMainLayout(): Container? = null

    fun createUi(init: WindowBase.() -> Unit)
}
