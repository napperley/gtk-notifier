package org.example.gtk_notifier

import kotlinx.cinterop.StableRef

internal interface StableRefExtension {
    val stableRef: StableRef<Any>

    fun fetchReference() = stableRef.asCPointer()

    fun disposeStableRef() {
        stableRef.dispose()
    }
}
